<?php



class MODEL_Ad_Banners extends SYS_Model_Database
{
	//--------------------------------------------------------------------------
	
	//public $name  = 'Рекламные компании';
	public $table = 'ad_banners';
//	public $date_fomat = 'd.m.Y - H:i';
//	
	public $thumbs = array(
		'thumb' => array(
			'size' => array(100, 100),
			'dist' => 'files/ad/s/',
			'crop' => 0
		),
		'img' => array(
			'size' => array(500, 500),
			'dist' => 'files/ad/x/',
			'crop' => FALSE
		),
	);
	
	//--------------------------------------------------------------------------
	
	function init()
	{
		$this->fields['ad_banners'] = array(
			'id'  => NULL,
			'cid' => NULL,
			'status' => array(
				'label'   => 'Статус',
				'default' => 1,
				'field'   => 'select',
				'options' => 'status_list',
				'user_group' => array(1)
			),
			// 'template_id' => array(
			// 	'label'   => 'Шаблон',
			// 	'default' => 1,
			// 	'field'   => 'select',
			// 	'options' => 'template_list'
			// ),
			'name' => array(
				'label' => 'Название',
				'field' => 'input',
				'rules' => 'trim|strip_tags|required',
			),
			'title' => array(
				'label' => 'Заголовок',
				'field' => 'input',
				'rules' => 'trim|strip_tags',
			),
			'body' => array(
				'label' => 'Содержание',
				'field' => 'html',
				'rules' => 'trim|required',
			),
			'url' => array(
				'label' => 'Ссылка',
				'field' => 'input',
				'rules' => 'trim|strip_tags|required',
			),
			'img' => array(
				'label'   => 'Изображение',
				'field'   => 'file',
				'rules'   => 'callback[ext.ad.banners_model.upload,img]',
			),
		);

		if (isset(sys::$config->ext_ad->thumbs))
		{
			$this->thumbs = sys::$config->ext_ad->thumbs;
		}
	}
	
	//--------------------------------------------------------------------------
	
	function upload($value, $callback, $field)
	{
		$key_value = $this->form->value('title') . $this->form->value('body');
		$key_error = $this->form->error('title');

		if ( ! ($key_value && ! $key_error)) return TRUE;
		if ( ! $value) return TRUE;

		$this->load->library('upload');
		$this->load->library('image');
		
		$this->upload->set_allowed_types('jpg');
		$this->upload->set_max_size(3);
		
		if ($result = $this->upload->run($field))
		{
			$file_name = md5($key_value) . '.jpg';
			
			$this->image->set_file_name($file_name);
			$this->image->process($result->full_path, $this->thumbs);
			
			return $file_name;// . '?' . time();
		}
		
		$this->form->set_error_message("callback[ext.ad.banners_model.upload,img]", $this->upload->error($field));
		return FALSE;
	}
		
	//--------------------------------------------------------------------------
	
	public function prepare_row_result(&$row)
	{	
		if ( ! empty($row->img))
		{
			foreach($this->thumbs as $name => $opt)
			{
				$row->$name = '/' . $opt['dist'] . ($row->img ? $row->img : $this->no_photo);
			}
		}
		
		return parent::prepare_row_result($row);
	}
	
	//--------------------------------------------------------------------------
	
	public function status_list($val = NULL)
	{
		static $list = array(
			0 => 'Отключен',
			1 => 'Включен'
		);
		
		if ($val !== NULL) return $list[$val];
		
		return $list;
	}
	
	//--------------------------------------------------------------------------
	
	public function template_list($val = NULL)
	{
		static $list = array(
			0 => 'Нет',
			1 => 'По умолчанию'
		);
		
		if ($val !== NULL) return $list[$val];
		
		return $list;
	}
	
	//--------------------------------------------------------------------------
}