<?php



class EXT_Ad_Context
{
	//--------------------------------------------------------------------------
	
	private $_src = array(
		'articles' => array(
			'link' => '/article/',
			'name' => 'Статьи'
		),
		'doit' => array(
			'name' => 'Вмешательства'
		),
		'enc' => array(
			'name' => 'Медицинский словарь'
		),
		'equipment' => array(
			'name' => 'Оборудование'
		),
		'illness' => array(
			'name' => 'Болезни'
		),
		'illness_sub' => array(
			'link' => array('/illness/%d/%d/', 'pid', 'index'),
			'name' => 'Болезни (подраздел)'
		),
		'inspect' => array(
			'name' => 'Обследования'
		),
		'symptoms' => array(
			'name' => 'Симптомы'
		),
		'trends' => array(
			'link' => array('/abs/%s/', 'key'),
			'key'  => 'key',
			'body' => 'description',
			'name' => 'Направления'
		),
	);
	
	//--------------------------------------------------------------------------
	
	public function __construct(&$ad)
	{
		$this->db =& sys::$lib->db;
	}
	
	//--------------------------------------------------------------------------
	
	public function add_link_by_keywords($url, $keywords, $pid = 0)
	{
		//echo $id;
		
		$total_insert = 0;
		
		foreach($keywords as $word)
		{
			$result = $this->find_words($word);
			
			if ($result['count'])
			{
				foreach ($result['result'] as $src_key => $com)
				{
					$param = $this->_src[$src_key];
					
//					$link = isset($param['link']) ? $param['link'] : "/{$table}/";
					$key  = isset($param['key'])  ? $param['key']  : "id";
//					$body = isset($param['body']) ? $param['body'] : 'body';
//					$where = "({$body} LIKE '%" . implode("%' OR {$body} LIKE '%", $query) . "%')";
					
					$exists = array();
					
					foreach ($com['data'] as $row)
					{
						//echo $row->link;
						//echo '<hr>';
						
						if ( ! empty($exists[$row->link])) continue;
						
						$exists[$row->link] = $this->db->where('page=?', $row->link)->count_all('ad_context_links');
						
						if (empty($exists[$row->link]))
						{
							$pos    = $row->words[0][1];
							$offset = strlen($row->words[0][0]);
							
							$this->add_links($pid, $src_key, $row->id, array(array('pos'=>$pos, 'offset'=>$offset)));
							
							$total_insert++;
							
//							echo '<pre>';
//							print_r( substr($row->body, $pos, $offset) );
//							echo "{$word} ";
//							echo($row->link);
//							echo('</pre><hr>');
						}
					}
					
					
					
					
//					echo '<pre>';

//					echo htmlspecialchars(print_r($com, 1));
//					echo('</pre><hr>');
				}
			}
			
		}
		
		return $total_insert;
	}
	
	//--------------------------------------------------------------------------
	
	public function find_all_links()
	{
		return $this->find(' href="', array(
			'prefix' => '/<a[^<]*',
			'suffix'=>'.*?<\/a>/i'
		));
	}
	
	//--------------------------------------------------------------------------
	
	public function find_links($query)
	{
		return $this->find($query, array(
			'prefix' => '/<a[^<]*',
			'suffix'=>'.*?<\/a>/i'
		));
	}
	
	//--------------------------------------------------------------------------
	
	public function find_words($query)
	{
		return $this->find($query, array(
			'prefix' => '/[^\s,.!?<>&)(:;"\'-]*',
			'suffix'=>'[^\s,.!?<>&)(:;"\'-]*/i'
		));
	}
	
	//--------------------------------------------------------------------------
	
	public function find($query, $preg)
	{
		$query = array_map('mb_strtolower', (array)$query);

		foreach ($query as $i=>&$row)
		{
			$row = trim($row);
			if ( ! $row) unset($query[$i]);
		}
//		SELECT * FROM illness WHERE body LIKE ('%??????%')
		
		$count  = 0;
		$pages  = 0;
		$result = $this->_src;
		foreach ($this->_src as $table => $param)
		{
			$link = isset($param['link']) ? $param['link'] : "/{$table}/";
			$key  = isset($param['key'])  ? $param['key']  : "id";
			$body = isset($param['body']) ? $param['body'] : 'body';
			$where = "({$body} LIKE '%" . implode("%' OR {$body} LIKE '%", $query) . "%')";
			$data = $this->db->where($where)->get($table)->result();
			foreach ($data as $i => &$row)
			{
				
				if (is_array($link))
				{
					$row->index += 2;
					$args = $link;
					foreach ($args as $i => $key) if ($i) $args[$i] = $row->$key;
					$row->link = call_user_func_array('sprintf', $args);
				}
				else
				{
					$row->link = $link . $row->$key . '/';
				}
				
				$row->index -= 2;
				$row->title = empty($row->title) ? $row->name : $row->title;
				$row->body  = $row->$body;
				$row->count = 0;
				$row->words = array();
				
				
				foreach ($query as $q_word)
				{
					$regexp = $preg['prefix'] . preg_quote($q_word, '/') . $preg['suffix'];
					preg_match_all($regexp, mb_convert_case($row->body, MB_CASE_LOWER, "UTF-8"), $matches, PREG_OFFSET_CAPTURE);
					
					$row->count += count(current($matches));
					$row->words = array_merge($row->words, current($matches));
				}
				
				$pages ++;
				$count += $row->count;
			}
			
			$result[$table]['data'] = $data;
		}
		
		return array('result'=>$result, 'count' => $count, 'pages' => $pages);
	}
	
	//--------------------------------------------------------------------------
	
	public function add_links($cid, $type, $row_id, $add_words)
	{
		if ( ! count($add_words)) return;
		$result = 0;
		$href = $this->db->where('id=?', $cid)->get('ad_context')->row()->url;
		
		$param = $this->_src[$type];
		
		$link = isset($param['link']) ? $param['link'] : "/{$type}/";
		$key  = isset($param['key'])  ? $param['key']  : "id";
		$body = isset($param['body']) ? $param['body'] : 'body';
		
		
		$page = $this->db->where('id=?', $row_id)->get($type)->row();
		$html = $page->$body;
		
		if (is_array($link))
		{
			$args = $link;
			foreach ($args as $i => $key) if ($i) $args[$i] = $page->$key;
			$link = call_user_func_array('sprintf', $args);
		}
		else
		{
			$link = $link . $page->$key . '/';
		}
		
		$g_offset = 0;
		foreach ($add_words as $word)
		{
			$result++;
			
			$anchor = substr($html, $word['pos'], $word['offset']+$g_offset);
			
			$str_l = '<a href="'.$href.'">';
			$html = substr_replace($html, $str_l, $word['pos']+$g_offset, 0);
			$g_offset += strlen($str_l);
			
			$str_r  = '</a>';
			$html = substr_replace($html, $str_r, $word['pos']+$word['offset']+$g_offset, 0);
			$g_offset += strlen($str_r);
			
			$data =  array(
				'pid'    => $cid,
				'href'   => $href,
				'anchor' => $anchor,
				'page'   => $link,
				'offset' => $word['pos']
			);
			
			$this->db->insert('ad_context_links', $data);
		}
		
		$this->db->where('id=?', $row_id)->update($type, array($body=>$html));
		//echo "<div style='background:#FFF; font-size:10px; padding:20px'>{$html}</div><hr>";
		return $result;
	}
	
	//--------------------------------------------------------------------------
	
	public function remove_link($com, $row_id, $offset)
	{
		$com_link  = "/{$com}/";
		$com_table = FALSE;
		$com_key   = 'id';
		$com_body  = 'body';
		
		foreach ($this->_src as $table => $param)
		{
			if ((isset($param['link']) && $param['link'] == $com_link) OR ($table == $com))
			{
				$com_body  = isset($param['body']) ? $param['body'] : 'body';
				$com_key   = isset($param['key'])  ? $param['key']  : "id";
				$com_table = $table;
				break;
			}
		}
		
		$old_html = $html = $this->db->where($com_key . '=?', $row_id)->get($com_table)->row()->$com_body;

		$r_offset  = strpos($html, '</a>', $offset) + 4;
		$html_link = substr($html, $offset, $r_offset - $offset);
		$anchor    = preg_replace('/.*?>(.*)<\/a>/i', '\1', $html_link);
		$html      = substr_replace($html, $anchor, $offset, $r_offset-$offset);
		
		/*
echo "{$anchor}<br>{$com}, {$row_id}, {$offset}  | " . (strlen($html_link) - strlen($anchor));
		echo '<code style="font-size:11px"><table width=100%><col width=50% /><col width=50% /><tr><td>';
		echo htmlspecialchars($old_html);
		echo '</td><td>';
		echo htmlspecialchars($html);
		echo '</td></tr></table></code><hr>';
		
*/
		$this->db->where($com_key . '=?', $row_id)->update($com_table, array($com_body => $html));
		
		return strlen($html_link) - strlen($anchor);
	}
	
	//--------------------------------------------------------------------------
	
	public function remove_links($cid, $type, $row_id, $remove_links)
	{
		if ( ! count($remove_links)) return;
		
		$result = 0;
		$link = $this->db->where('id=?', $cid)->get('ad_context')->row()->url;
		
		$body = empty($this->_src[$type]['body']) ? 'body' : $this->_src[$type]['body'];
		
		$html = $this->db->where('id=?', $row_id)->get($type)->row()->$body;

		$g_offset = 0;
		foreach ($remove_links as $link)
		{
			$result++;

			$html_link = substr($html, $link['pos']+$g_offset, $link['offset']);
			$anchor    = preg_replace('/.*?>(.*)<\/a>/i', '\1', $html_link);
			$html = substr_replace($html, $anchor, $link['pos']+$g_offset, $link['offset']);
			$g_offset -= strlen($html_link) - strlen($anchor);
		}
		
		$this->db->where('id=?', $row_id)->update($type, array($body=>$html));
		//echo "<div style='background:#FFF; font-size:10px; padding:20px'>{$html}</div><hr>";
		return $result;
	}
	
	//--------------------------------------------------------------------------
}