<div class="sub_tabs">
	<a href="/admin/ad/banners/<?=$compaing->id ?>/">Типы баннеров</a>
	<a href="/admin/ad/<?=$compaing->id ?>/" class="selected">Размещение</a>
	<a href="/admin/ad/stat/<?=$compaing->id ?>/">Статистика</a>
</div>


<?php

function rel_date($time)
{
	static $today;
	if ($today === NULL)
	{
		$date = new dateTime;
		$date->setTime(0, 0);
		$today = $date->format('U');
	}
//	86400
	
//	echo 60 * 60 * 24;
//	die();

	switch (TRUE)
	{
		case $today <= $time: return 'сегодня';
		case $today - 86400 <= $time: return 'вчера';
		case $today - 86400 * 2 <= $time: return '3 дня назад';
		case $today - 86400 * 3 <= $time: return '4 дня назад';
		case $today - 86400 * 4 <= $time: return '5 дня назад';
		case $today - 86400 * 5 <= $time: return '6 дня назад';
		case $today - 86400 * 6 <= $time: return '7 дня назад';
	}
	
	return date('d.m.Y', $time);
}

?>

<h1><?=$compaing->title ?>: Размещение</h1>

<table class="data_table">
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<col />
	<? $bid = 0 ?>
	<? foreach ($banners_place as $i => $b): ?>
		<? if ($bid != $b->bid): ?>
			<tr class="group">
				<td colspan="16">
					<img src="/files/ad/s/<?=$banners[$b->bid]->img ?>" alt="" style="float:right; margin:0 0 0 10px;" />
					<a style="font-size:15px; color:#CCC" href="/admin/ad/banners/<?=$compaing->id ?>/edit/<?=$b->bid ?>">#<?=$b->bid ?> <?=$banners[$b->bid]->name ?></a>
					<div style="margin:5px 0 0 0">
						<b><?=$banners[$b->bid]->title ?></b> &bull; <?=$banners[$b->bid]->url ?><br>
						<?=$banners[$b->bid]->body ?>
					</div>
				</td>
			</tr>
			<tr>
				<th>ID</th>
				<th>Включен</th>
				<th>Размещение</th>
				<th>Раздел</th>
				<th colspan="3">Клики</th>
				<th colspan="3">Показы</th>
				<th colspan="3">CTR</th>
				<th></th>
			</tr>
		<? endif ?>
		<tr class="<?=$i%2==0 ? 'a' : 'b' ?>" style="font-weight:bold">
			<td>
				<a href="/admin/ad/page_stat/<?=$compaing->id ?>/<?=$b->id ?>/"><?=$b->id ?></a>
			</td>
			<td><?=$b->status ? 'Да' : 'Нет' ?></td>
			<td><?=$model->place_list($b->place) ?></td>
			<td>
				<?=$model->page_list($b->page) ?> 
				<? if ($b->index_only || $b->relation): ?>
					(<?=$b->index_only ? 'главная' : '' ?><?=$b->relation ? $b->relation : '' ?>)</td>
				<? endif ?>
			
			<? $s  = $this->get_stat($b->id) ?>
			<? $sa = $this->get_stat_all($b->id) ?>
			
			<td style="color:#900; text-align:right"><?=!$sa ? NULL : $sa->clicks ?></td>
			<td style="text-align:right"><?=!$s ? NULL : $s[0]->clicks ?></td>
			<td style="text-align:right"><?=!$s ? NULL : $s[1]->clicks ?></td>
			<!-- <td style="text-align:right"><?=!$s ? NULL : $s[2]->clicks ?></td> -->
			

			<td style="color:#900; text-align:right"><?=!$sa ? NULL : $sa->views ?></td>
			<td style="color:#999; text-align:right"><?=!$s ? NULL : $s[0]->views ?></td>
			<td style="color:#999; text-align:right"><?=!$s ? NULL : $s[1]->views ?></td>
			<!-- <td style="color:#999; text-align:right"><?=!$s ? NULL : $s[2]->views ?></td> -->
			
			<td style="text-align:right"><?=!$s ? NULL : $s[0]->views ? number_format($s[0]->clicks/$s[0]->views, 3, '.', '') : '-' ?></td>
			<td style="text-align:right"><?=!$s ? NULL : $s[1]->views ? number_format($s[1]->clicks/$s[1]->views, 3, '.', '') : '-' ?></td>
			<td style="text-align:right"><?=!$s ? NULL : $s[2]->views ? number_format($s[2]->clicks/$s[2]->views, 3, '.', '') : '-' ?></td>
			
			<td>
				<a title="Изменить" href="/admin/ad/<?=$compaing->id ?>/edit/<?=$b->id ?>"><img src="/extensions/admin/ico/pencil.png" alt="" /></a> 
				<a style="color:#900" title="Удалить" href="/admin/ad/<?=$compaing->id ?>/remove/<?=$b->id ?>"><img src="/extensions/admin/ico/cross-script.png" alt="" /></a>
			</td>
		</tr>
		
		<? $bid = $b->bid ?>
		
	<? endforeach ?>
	
	
</table>

<?=h_form::open('#add_form', 'POST', 'id="add_form"') ?>

	<?=$this->form->form_errors() ?>
	
	<table>
		<tr>
			<td>
				<?=$this->form->label('bid') ?><br>
				<? //$this->form->set_field('bid', 'multiselect') ?>
				<?=$this->form->field('bid') ?>
			</td>
			<td>
				<?=$this->form->label('place') ?><br>
				<? //$this->form->set_field('place', 'multiselect') ?>
				<?=$this->form->field('place') ?>
			</td>
			<td>
				<?=$this->form->label('page') ?><br>
				<? //$this->form->set_field('page', 'multiselect') ?>
				<?=$this->form->field('page') ?>
			</td>
			<td>
				<?=$this->form->label('template_id') ?><br>
				<?=$this->form->field('template_id') ?>
			</td>
			<td>				
				<?=$this->form->label('relation') ?><br>
				<? //$this->form->set_field('relation', 'input') ?>
				<?=$this->form->field('relation', NULL, 'style="width:100px"') ?>
			</td>
		</tr>
	</table>
	<hr>
	<?=$this->form->label('index_only') ?>
	<?=$this->form->field('index_only') ?>
	<hr>
	<?=h_form::submit('Добавить') ?>
	
<?=h_form::close() ?>