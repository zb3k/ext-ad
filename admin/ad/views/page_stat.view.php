<table class="data_table">
<? foreach ($stat as $i => $row): ?>
<? if (preg_match('@\?@ui', $row->url)) continue; ?>
<tr class="<?=$i%2==0 ? 'a' : 'b' ?>">
	<td><?=$row->url ?></td>
	<td><?=$row->clicks ?></td>
	<td><?=$row->views ?></td>
	<td><?=number_format($row->clicks / $row->views,2) ?></td>
</tr>
<? endforeach ?>
</table>