<h1>Редактирование баннера</h1>

<?=h_form::open_multipart() ?>

	<?=$this->form->form_errors() ?>

	<?=$this->form->label('name') ?><br>
	<?=$this->form->field('name') ?>
	<br>
	<?=$this->form->label('title') ?><br>
	<?=$this->form->field('title') ?>
	<br>
	<?=$this->form->label('url') ?><br>
	<?=$this->form->field('url') ?>
	<br>
	<?=$this->form->label('img') ?><br>
	<?=$this->form->field('img') ?>
	<br>
	<?=$this->form->label('template_id') ?><br>
	<?=$this->form->field('template_id') ?>
	<br>
	<?=$this->form->label('body') ?><br>
	<?=$this->form->field('body') ?>
	<br><br>
	<?=h_form::submit('Сохранить') ?>
	
<?=h_form::close() ?>