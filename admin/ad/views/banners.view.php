<div class="sub_tabs">
	<a href="/admin/ad/banners/<?=$compaing->id ?>/" class="selected">Типы баннеров</a>
	<a href="/admin/ad/<?=$compaing->id ?>/">Размещение</a>
	<a href="/admin/ad/stat/<?=$compaing->id ?>/">Статистика</a>
</div>

<h1><?=$compaing->title ?>: Типы баннеров</h1>

<table class="data_table">
	<tr>
		<th>ID</th>
		<th>Название</th>
		<th>Включен</th>
		<th>Содержание</th>
		<th></th>
	</tr>
	<? foreach ($banners as $i => $b): ?>
	<tr class="<?=$i%2==0 ? 'a' : 'b' ?>">
		<td><?=$b->id ?></td>
		<td><?=$b->name ?></td>
		<td><?=$b->status ? 'Да' : 'Нет' ?></td>
		<td>
			<? if (trim($b->thumb)): ?>
				<img src="<?=$b->thumb ?>" alt="" style="float:left; margin:5px 5px 5px 0">
			<? endif ?>
			Заголовок: <b><?=$b->title ?></b><br>
			Внутрення ссылка: <b>/go/b/<?=$b->id ?>/</b><br />
			Внешняя ссылка: <b><?=$b->url ?></b><br>
			<?=$b->body ?>
		</td>
		<td width="120">
			<a href="/admin/ad/banners/<?=$compaing->id ?>/edit/<?=$b->id ?>"><img src="/extensions/admin/ico/pencil.png" alt=""  style="vertical-align:middle" />&nbsp;Редактировать</a><br />
			<a onclick="return confirm('Удалить баннер?')" href="/admin/ad/banners/<?=$compaing->id ?>/remove/<?=$b->id ?>"><img src="/extensions/admin/ico/cross-script.png" alt=""  style="vertical-align:middle" />&nbsp;Удалить</a>
		</td>
	</tr>
	<? endforeach ?>
</table>

<?=h_form::open_multipart('#add_form', 'POST', 'id="add_form"') ?>

	<?=$this->form->form_errors() ?>
	
		<?=$this->form->label('name') ?><br>
		<?=$this->form->field('name') ?>
		<br>
		<?=$this->form->label('title') ?><br>
		<?=$this->form->field('title') ?>
		<br>
		<?=$this->form->label('url') ?><br>
		<?=$this->form->field('url') ?>
		<br>
		<?=$this->form->label('img') ?><br>
		<?=$this->form->field('img') ?>
		<!-- <br>
		<?=$this->form->label('template_id') ?><br>
		<?=$this->form->field('template_id') ?> -->
		<br>
		<?=$this->form->label('body') ?><br>
		<?=$this->form->field('body') ?>
	
	<br><br>
	<?=h_form::submit('Добавить') ?>
	
<?=h_form::close() ?>
</div>

<?=htmlspecialchars('<a href="%LINK%">...</a>') ?><br />
<?=htmlspecialchars('<a href="#" onclick="return go(this, \'/go/711/\')">...</a>') ?>