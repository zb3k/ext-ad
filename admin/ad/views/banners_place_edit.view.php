<h1>Редактирование размещения баннера</h1>

<br><a href="/admin/ad/<?=$banner_place->cid ?>">Назад к списку</a>

<?=h_form::open('#add_form', 'POST', 'id="add_form"') ?>

	<?=$this->form->form_errors() ?>

	<table>
		<tr>
			<td>
				<?=$this->form->label('bid') ?><br>
				<? $this->form->set_field('bid', 'multiselect') ?>
				<?=$this->form->field('bid', NULL, 'style="height:250px"') ?>
			</td>
			<td>
				<?=$this->form->label('place') ?><br>
				<? $this->form->set_field('place', 'multiselect') ?>
				<?=$this->form->field('place', NULL, 'style="height:250px"') ?>
			</td>
			<td>
				<?=$this->form->label('trend') ?><br>
				<? $this->form->set_field('trend', 'multiselect') ?>
				<?=$this->form->field('trend', NULL, 'style="height:250px"') ?>
			</td>
			<td>
				<?=$this->form->label('page') ?><br>
				<? $this->form->set_field('page', 'multiselect') ?>
				<?=$this->form->field('page', NULL, 'style="height:250px"') ?>
			</td>
			<td>				
				<?=$this->form->label('relation') ?><br>
				<? $this->form->set_field('relation', 'textarea') ?>
				<?=$this->form->field('relation', NULL, 'style="height:244px; width:100px"') ?>
			</td>
		</tr>
	</table>
	<hr>
	<?=$this->form->label('index_only') ?>
	<?=$this->form->field('index_only') ?>
	<hr>
	<?=h_form::submit('Сохранить') ?>
	
<?=h_form::close() ?>