<div class="sub_tabs">
	<a href="/admin/ad/banners/<?=$compaing->id ?>/">Типы баннеров</a>
	<a href="/admin/ad/<?=$compaing->id ?>/">Размещение</a>
	<a href="/admin/ad/stat/<?=$compaing->id ?>/" class="selected">Статистика</a>
</div>


<h1><?=$compaing->title ?>: Статистика</h1>

<!--table class="data_table">
	<tr>
		<td>Всего показов:</td><td><?=$global_stat['views'] ?></td>
	</tr>
	<tr>
		<td>Всего кликов:</td><td><?=$global_stat['clicks'] ?></td>
	</tr>
</table-->
<br>
<?=h_form::open() ?>
	
	<table>
		<tr>
			<td>
				<div id="calendar"></div>
				<input type="hidden" name="date_from" id="date_from" value="<?=$date_from ?>" />
				<input type="hidden" name="date_to" id="date_to" value="<?=$date_to ?>" />
				<br>
				<button type="submit">показать</button>
			</td>
		</tr>
	</table>
	
<?=h_form::close() ?>
<script type="text/javascript">
$('#calendar').DatePicker({
	flat: true,
	date: [$('#date_from').val(), $('#date_to').val()],
	current: $('#date_to').val(),
	calendars: 3,
	mode: 'range',
	format: 'd.m.Y',
	locale: {
		days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
		daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
		daysMin: ["вс", "пн", "вт", "ср", "чт", "пт", "сб", "вс"],
		months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		weekMin: ''
	},
	starts: 1,
	onChange: function(d){
		$('#date_from').val(d[0]);
		$('#date_to').val(d[1]);
	}
}).DatePickerShow();
</script>
<br><br><br>
<? if (count($stat)): ?>
	<b><?=date('d.m.Y',$stat[0]->day_time) ?> - <?=date('d.m.Y',$stat[count($stat)-1]->day_time) ?></b>
	<? require_once(APP_PATH . 'libraries/gchart' . EXT) ?>
	<?php
	$lineChart = new gLineChart(500,200);
	$lineChart->addDataSet(array(112,315,66,40));
	//$lineChart->setLegend(array("first", "second", "third","fourth"));
	$lineChart->setColors(array("ff3344", "11ff11", "22aacc", "3333aa"));
	$lineChart->setVisibleAxes(array('x','y'));
	$lineChart->setDataRange(30,400);
	$lineChart->addAxisLabel(0, array("This", "axis", "has", "labels!"));
	$lineChart->addAxisRange(1, 30, 400);
	$lineChart->setStripFill('bg',0,array('CCCCCC',0.15,'FFFFFF',0.1));
	?>
	<!--img src="<?=$lineChart->getUrl();  ?>" /-->
	
<? endif ?>




<? $i=0; $t_clicks = 0; $t_views = 0; ?>
<table class="data_table">
	<tr>
		<th>Дата</th>
		<th>Клики</th>
		<th>Показы</th>
	</tr>
<? foreach ($stat as $i=>$row): ?>
	<? $t_clicks += $row->clicks; $t_views += $row->views; ?>
	<tr class="<?=$i%2==0?'a':'b' ?>">
		<td><?=date('d.m.Y', $row->day_time) ?></td>
		<td><?=$row->clicks ?></td>
		<td><?=$row->views ?></td>
	</tr>
<? endforeach ?>
	<tr class="<?=($i+1)%2==0?'a':'b' ?>">
		<td><b>Всего за период</b></td>
		<td><b><?=$t_clicks ?></b></td>
		<td><b><?=$t_views ?></b></td>
	</tr>
</table>