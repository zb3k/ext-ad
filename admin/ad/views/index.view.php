<h1>Рекламные компании</h1>

<table class="data_table">
	<tr>
		<th>ID</th>
		<th>Название компании</th>
		<th></th>
		<th>Статус</th>
		<th></th>
		<th></th>
		<!--th>Включен</th>
		<th>Дата создания</th>
		<th>Начало показа</th>
		<th>Окончание показа</th-->
		<th width="10">Тенденция</th>
		<th colspan="2" width="170">Сегодня (клик/показ)</th>
		<th colspan="2" width="170">Вчера (клик/показ)</th>
		<th colspan="2" width="170">3 дня н. (клик/показ)</th>
	</tr>
	<? foreach ($compaings as $i => $c): ?>
	<tr class="<?=$i%2==0 ? 'a' : 'b' ?>">
		<td><?=$c->id ?></td>
		<td>
			<a href="/admin/ad/<?=$c->id ?>"><?=$c->title ?></a>
		</td>
		<td>
			<a href="/admin/ad/edit/<?=$c->id ?>/"><img src="/extensions/admin/ico/pencil.png" alt="" /></a>
		</td>
		<td>
			<? if ($c->status): ?>
				<img src="/extensions/admin/ico/status.png" alt=""  style="vertical-align:middle" />
				<a href="/admin/ad/set_compaign_status/<?=$c->id ?>/0/">ВЫКЛ</a>
			<? else: ?>
				<img src="/extensions/admin/ico/status-busy.png" alt="" style="vertical-align:middle" />
				<a href="/admin/ad/set_compaign_status/<?=$c->id ?>/1/">ВКЛ</a>
			<? endif ?>
		</td>
		<td>
			<a href="/admin/ad/banners/<?=$c->id ?>/">Баннеры</a>
		</td>
		<td>
			<a href="/admin/ad/stat/<?=$c->id ?>/">Статистика</a>
		</td>
		<!--td><?=$c->status ? 'Да' : 'Нет' ?></td>
		<td><?=$c->postdate ?></td>
		<td><?=$c->begindate ?></td>
		<td><?=$c->enddate ?></td-->
		<? $d0 = $stat[$c->id][$day0] ?>
		<? $d1 = $stat[$c->id][$day1] ?>
		<? $d2 = $stat[$c->id][$day2] ?>
		<? $d3 = $stat[$c->id][$day3] ?>
		
		<!-- Today -->
		<td>
			<? $diff = $d0->clicks*$d1->clicks>0 ? floor($d1->views/$d1->clicks - $d0->views/$d0->clicks) : '?' ?>
			<span style="color:#<?=$diff>0?'5C5':($diff<0?'C00':'999')?>; float:right"><?=($diff>0?'+':'') . $diff ?></span>
		</td>
		<td style="color:#<?=$d0->clicks ? '000' : 'CCC' ?>">
			<b>
			<? if ($d0->clicks > $d1->clicks): ?>
				<span style="color:#5C5; float:right">+<?=$d0->clicks - $d1->clicks ?></span>
			<? endif ?>
			<?=$d0->clicks ?>
			</b>
		</td>
		
		<td style="color:#<?=$d0->views ? '999' : 'CCC' ?>">
			<? if ($d0->views > $d1->views): ?>
				<span style="color:#9C9; float:right">+<?=$d0->views - $d1->views ?></span>
			<? endif ?>
			<?=$d0->views ?>
		</td>
		
		<!-- Yesterday -->
		<td style="color:#<?=$d1->clicks ? '000' : 'CCC' ?>">
			<b>
			<?=$d1->clicks ?>
			<? if ($d1->clicks > $d2->clicks): ?>
				<span style="color:#5C5; float:right">+<?=$d1->clicks - $d2->clicks ?></span>
			<? endif ?>
			</b>
		</td>
		<td style="color:#<?=$d1->views ? '999' : 'CCC' ?>">
			<? if ($d1->views > $d2->views): ?>
				<span style="color:#9C9; float:right">+<?=$d1->views - $d2->views ?></span>
			<? endif ?>
			<?=$d1->views ?>
		</td>
		
		<!-- 3 day ago -->
		<td style="color:#<?=$d2->clicks ? '000' : 'CCC' ?>">
			<b>
			<?=$d2->clicks ?>
			<? if ($d2->clicks > $d3->clicks): ?>
				<span style="color:#5C5; float:right">+<?=$d2->clicks - $d3->clicks ?></span>
			<? endif ?>
			</b>
		</td>
		<td style="color:#<?=$d2->views ? '999' : 'CCC' ?>">
			<? if ($d2->views > $d3->views): ?>
				<span style="color:#9C9; float:right">+<?=$d2->views - $d3->views ?></span>
			<? endif ?>
			<?=$d2->views ?>
		</td>
		
	</tr>
	<? endforeach ?>
</table>


<?=h_form::open('#add_form', 'POST', 'id="add_form"') ?>

	<?=$this->form->form_errors() ?>

	<?=$this->form->label('title') ?>
	<?=$this->form->field('title') ?>
	
	<?=h_form::submit('Создать') ?>
	
<?=h_form::close() ?>