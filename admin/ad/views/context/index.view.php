<h1>Список проектов</h1>

<table class="data_table">
	<tr>
		<th><a href="?order=id&desc=<?=(int)($ofield=='id' ? !$otype : 0)?>"><?=$ofield=='id'?(!$otype?'&darr;':'&uarr;'):''?> ID</a></th>
		<th><a href="?order=name&desc=<?=(int)($ofield=='name' ? !$otype : 0)?>"><?=$ofield=='name'?(!$otype?'&darr;':'&uarr;'):''?> Имя проекта</a></th>
		<th><a href="?order=url&desc=<?=(int)($ofield=='url' ? !$otype : 0)?>"><?=$ofield=='url'?(!$otype?'&darr;':'&uarr;'):''?> URL</a></th>
		<th><a href="?order=count_links&desc=<?=(int)($ofield=='count_links' ? !$otype : 0)?>"><?=$ofield=='count_links'?(!$otype?'&darr;':'&uarr;'):''?> Всего ссылок</a></th>
		<th>Ключевые слова</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<? foreach ($ad_list as $i => $row): ?>
	<tr class="<?=$i%2==0?'a':'b'?>">
		<? if ($row->id==0): ?>
			<td></td>
			<td><i style="color:#999"><b>Ссылки вне проектов</b></i></td>
			<td></td>
			<td class="tar">
				<a href="/admin/ad/context/view/<?=$row->id ?>/"><?=$row->count_links ?></a>
			</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		<? else: ?>
			<td><?=$row->id ?></td>
			<td><?=$row->name ?></td>
			<td><?=$row->url ?></td>
			<td class="tar">
				<a href="/admin/ad/context/view/<?=$row->id ?>/"><?=$row->count_links ?></a>
				<!--a href="/admin/ad/context/add/<?=$row->id ?>/">Добавить</a-->
			</td>
			<td>
				<? $words = (array)unserialize($row->settings) ?>
				<? foreach($words as $r): ?>
					<? if ($r[0]): ?>
						<span title="<?=empty($r[1])?'':implode(', ', $r) ?>" style="background:#FC0; padding:0 3px; border-radius:3px; color:#520"><?=$r[0] ?><?=$r[1]?'&nbsp;[…]':''?></span>
					<? endif ?>
				<? endforeach ?>
			</td>
			<td><a href="/admin/ad/context/settings/<?=$row->id ?>/">Настройки</a></td>
			<td><a href="/admin/ad/context/manual_cron/<?=$row->id ?>/">Проставить ссылки</a></td>
			<td><a onclick="return confirm('Вы уверены?')" href="/admin/ad/context/remove/<?=$row->id ?>/">Удалить</a></td>
		<? endif ?>
	</tr>
	<? endforeach ?>
</table>

<br>
<a style="color:#C20" class="button" href="/admin/ad/context/update/">Найти ссылки*</a>
<a style="color:#C20" class="button" href="/admin/ad/context/manual_cron/">Проставить ссылки**</a>
<br>
<br>

<form action="" method="post" id="add_form">
	<b>Добавление нового проекта:</b>
	<table style="margin:10px 0">
		<tr>
			<td class="vam"><?=$this->form->label('name') ?> </td>
			<td><?=$this->form->field('name') ?></td>
		</tr>
		<tr>
			<td class="vam"><?=$this->form->label('url') ?> </td>
			<td><?=$this->form->field('url') ?></td>
		</tr>
	</table>
	
	<button type="submit">Добавить</button>
</form>


<br />
<small>* - Поиск и обновление ссылок по всему сайту</small><br />
<small>** - Автоматическое добавление ссылок по ключевым словам указанным в настройках проека</small><br />
<br /><br />