<? if ($ad_context->id): ?>
	<h1><?=$ad_context->name ?> - URL: <?=$ad_context->url ?></h1>
<? else: ?>
	<h1>Ссылки вне проектов</h1>
<? endif ?>

<? if (count($links)): ?>

	<table class="data_table">
		<tr>
			<th><a href="?order=id&desc=<?=(int)($ofield=='id' ? !$otype : 0)?>"><?=$ofield=='id'?(!$otype?'&darr;':'&uarr;'):''?> ID</a></th>
			<th><a href="?order=page&desc=<?=(int)($ofield=='page' ? !$otype : 0)?>"><?=$ofield=='page'?(!$otype?'&darr;':'&uarr;'):''?> Страница</a></th>
			<th><a href="?order=anchor&desc=<?=(int)($ofield=='anchor' ? !$otype : 0)?>"><?=$ofield=='anchor'?(!$otype?'&darr;':'&uarr;'):''?> Текст ссылки</a></th>
			<th><a href="?order=href&desc=<?=(int)($ofield=='href' ? !$otype : 0)?>"><?=$ofield=='href'?(!$otype?'&darr;':'&uarr;'):''?> Ссылка</a></th>
		</tr>
	
		<? foreach ($links as $i => $row): ?>
		<tr class="<?=$i%2?'a':'b'?>">
			<td><?=$i+1 ?></td>
			<td><a href="<?=$row->page ?>" target="_blank"><?=$row->page ?></a></td>
			<td><?=htmlspecialchars($row->anchor) ?></td>
			<td><a href="<?=$row->href ?>" target="_blank"><?=$row->href ?></a></td>
		</tr>
		<? endforeach ?>

<? endif ?>