<h1>Настройки проекта (<?=$ad_context->name ?>: <code><?=$ad_context->url ?></code>)</h1>

<form action="" method="post">
	
	<table class="data_table">
		<col width="120" />
		<col />

		<tr class="a">
			<td class="vam">Название проекта</td>
			<td><input type="text" name="name" style="width:150px" value="<?=$ad_context->name ?>" /></td>
		</tr>
		
		<tr class="b">
			<td class="vam">URL</td>
			<td><input type="text" name="url" style="width:150px" value="<?=$ad_context->url ?>" /></td>
		</tr>
		
	</table>

	
	<br />
	<h1>Ключевые слова</h1>
	<table width="100%" class="data_table">
		<col width="1" />
		<col width="160" />
		<col />
		<tr>
			<th></th>
			<th>Ключевое слово</th>
			<th>Варианты написания (через запятую)</th>
		</tr>
		<? for ($i=0; $i<count($settings)+5; $i++): ?>
			<? $row = empty($settings[$i]) ? array() : $settings[$i] ?>
			<? $key = $row ? array_shift($row) : '' ?>

			<tr class="<?=$i%2==0?'a':'b' ?>">
				<td><?=$i+1 ?></td>
				<td><input type="text" name="key[]" style="width:150px" value="<?=$key ?>" /></td>
				<td><input type="text" name="variants[]" style="width:100%" value="<?=implode(', ', $row) ?>" /></td>
			</tr>
		<? endfor ?>
	</table>
	<button type="submit" style="color:#C50"><b>Сохранить</b></button>
	<a href="/admin/ad/context/" class="button">Вернуться к списку</a>
</form>
