<form action="" method="post" style="background:#BCD; padding:10px; margin-bottom:10px">
	
	Список запросов (запрос с каждой строки):<br />
	<textarea style="width:500px; height:50px;" name="query"><?=@$_POST['query'] ?></textarea><br />
	
	<button type="submit">Найти</button>
	
</form>


<? if ( ! empty($message)): ?>
	<div class="message"><?=$message ?></div>
<? endif ?>


<? if ($result['result']): ?>
	
	<b>Всего найдено.</b> Фраз: <?=$result['count'] ?>, Страниц: <?=$result['pages'] ?>
	<br /><br />
	<form action="" method="post">
	<button type="submit">Добавить</button>
	<label><input type="checkbox" onchange="$('label:first-child input[type=checkbox]').attr('checked', this.checked ? 'checked' : false)" /> Первое вхождение</label>
	<table class="data_table">
		<tr>
			<th>#</th>
			<th>URL</th>
			<th>Название страницы</th>
			<th>Найденные слова</th>
			<th>Кол-во</th>
		</tr>
	<? foreach ($result['result'] as $table => $section): ?>
		<? if ( ! empty($section['data'])): ?>
		<tr>
			<td colspan="5" style="color:#777; font-weight:bold; padding:10px 5px 3px; border-bottom:2px solid #CCC; border-top:1px solid #CCC">
				<?=$section['name'] ?>
			</td>
		</tr>
		<? endif ?>
		<? foreach ($section['data'] as $i => $row): ?>
		<tr class="<?=$row->index%2?'a':'b'?>">
			<td><?=$row->index ?></td>
			<td><a href="<?=$row->link ?>" target="_blank"><?=$row->link ?></a></td>
			<td title="<?=$row->title ?>"><?=hlp::cut_text($row->title, 30) ?></td>
			<td>
				<? foreach ($row->words as $i=>$word): ?>
					<? $l_pos  = $word[1] ?>
					<? $offset = strlen($word[0]) ?>
					
					<label>
						<input type="checkbox" name="links[]" value="<?="{$table}-{$row->id}-{$l_pos}-{$offset}" ?>"/>
						<?=mb_substr(htmlspecialchars(substr($row->body, $l_pos-100, 100)), -50)
						?><b style="color:#C00"><?=htmlspecialchars(substr($row->body, $l_pos, $offset)) ?></b><?=
						mb_substr(htmlspecialchars(substr($row->body, $l_pos+$offset, 50)), 0 , 25) ?><br />
					</label>
				<? endforeach ?>
			</td>
			<td><?=$row->count ?></td>
		</tr>
		<? endforeach ?>
	<? endforeach ?>
	</table>
	<input type="hidden" name="add_links" value="1" />
	<button type="submit">Добавить</button>
	</form>
<? endif ?>