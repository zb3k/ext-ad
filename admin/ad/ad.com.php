<?php


class COM_Ad extends SYS_Component
{
	//--------------------------------------------------------------------------
	
	function main_init()
	{
		$this->link = '/admin/ad/';
		$this->sub  = '';
		//$this->model = new MODEL_;
	}
	
	//--------------------------------------------------------------------------
	
	function act_context($sub_action = 'index')
	{
		$this->sub = 'context';
		
		$sub_action_fn = 'suba_' . $sub_action;
		
		if (method_exists(&$this, $sub_action_fn))
		{
			$args = func_get_args();
			array_shift($args);
			$this->data['view'] = $sub_action;
			call_user_func_array(array(&$this, $sub_action_fn), $args);
		}
	}
	
	//--------------------------------------------------------------------------
	
	function suba_index()
	{
		$ofield = empty($_GET['order']) || preg_replace('/[a-z_]+/i', '', $_GET['order']) ? 'name' : $_GET['order'];
		$otype  = empty($_GET['desc']) ? '' : ' DESC';
		
		$this->load->library('form');
		$this->form->set_field('name', 'input', 'Имя', 'trim|required');
		$this->form->set_field('url', 'input', 'URL', 'trim|required');
		
		if ($this->form->validation())
		{
			$this->db->insert('ad_context', array('name'=>$_POST['name'], 'url'=>$_POST['url'], 'status'=>1));
		}
		
		$this->db->select('ad_context.*, COUNT(ad_context_links.id) AS count_links')->join('ad_context_links', 'ad_context_links.pid = ad_context.id');
		$this->data['ad_list'] = $this->db->group_by('ad_context.id')->order_by($ofield . $otype)->get('ad_context')->result();
		
		$this->data['ofield'] = $ofield;
		$this->data['otype']  = $otype;
	}
	
	//--------------------------------------------------------------------------
	
	function suba_remove_doubles()
	{
		$this->view = FALSE;
	}
	
	//--------------------------------------------------------------------------
	
	function suba_remove($id)
	{
		$this->view = FALSE;
		
		
		$result = $this->db->where('pid=?', $id)->get('ad_context_links')->result();
//		print_r($result);exit;
		$plinks = array();
		foreach ($result as $row) $plinks[$row->page][] = $row;

		foreach ($plinks as $page => $links)
		{
			$g_offset = 0;
			foreach ($links as $row)
			{
				//echo "[[[ {$row->offset} | {$g_offset} ]]]<br>";
				preg_match('/\/([^\/]+)\/([^\/]+)\//i', $row->page, $matches);
				$com    = $matches[1];
				$row_id = $matches[2];
				$g_offset += $this->ad->context->remove_link($com, $row_id, $row->offset - $g_offset);
			}
		}
		
		$this->db->where('id=?', $id)->delete('ad_context');
		
		ob_get_level() && ob_clean();
		header('Location: /admin/ad/context/');
		exit;
	}
	
	//--------------------------------------------------------------------------
	
	function suba_manual_cron($cid = 0)
	{
		if ($cid) $this->db->where('id=?', $cid);
		$context = $this->db->get('ad_context')->result();
		
		$result = array();
		
		foreach ($context as $i => &$row) 
		{
			$row->keywords = array();
			$keywords_res = (array)unserialize($row->settings);
			
			$result[$row->url] = 0;
			
			
			foreach ($keywords_res as $group)
			{
				$group = (array)$group;
				
				foreach ($group as $key)
				{
					$key = (array)$key;
					foreach ($key as $k)
					{
						if ($k)
						{
							$k = mb_strtolower($k);
							$context[$i]->keywords[$k] = $k;
						}
					}
				}
				
			}
			
			if (count($row->keywords))
			{
				
				 $result[$row->url] += $this->ad->context->add_link_by_keywords($row->url, $row->keywords, $row->id);
			}
		}
		
		$this->data['result'] = $result;
	}
	
	//--------------------------------------------------------------------------
	
	function suba_update()
	{
		$result = $this->db->get('ad_context')->result();
		
		$context = array();
		
		foreach ($result as $row)
		{
			$context[$row->url] = $row;
		}
		
		$data = $this->ad->context->find_all_links();
		
		$data['remove_links'] = 0;
		
		$this->db->query('TRUNCATE TABLE ad_context_links');
		
		foreach ($data['result'] as $table => &$result)
		{
			foreach ($result['data'] as $k=>&$row)
			{
				$g_offset = 0;
				
				foreach($row->words as $i=>&$link)
				{
					preg_match('/<a [^>]*href=[\'"](.*?)[\'"][^>]*>(.*?)<\/a>/i', $link[0], $matches);

					$link = array(
						'href'   => $matches[1],
						'anchor' => $matches[2],
						'offset' => $link[1],
					);
					
					
					if ( strpos($link['anchor'], '<a ') !== FALSE )
					{
						preg_match('/\/([^\/]+)\/([^\/]+)\//i', $row->link, $matches);
						$com    = $matches[1];
						$row_id = $matches[2];
						$g_offset += $this->ad->context->remove_link($com, $row_id, $link['offset'] - $g_offset);
						$data['remove_links']++;
					}
					
					
					$href = $link['href'];
					
					$pid = 0;
					
					if (isset($context[$href]))
					{
						$pid = $context[$href]->id;
					}
					elseif ($href{0} != '/')
					{
						foreach (sys::$config->sys->aliases as $alias)
						{
							$new_href = str_replace($alias, '', $href);

							if (isset($context[$new_href]))
							{
								$pid = $context[$new_href]->id;
							}
						}
					}
					
					$this->db->insert('ad_context_links', array(
						'pid'    => $pid,
						'href'   => $link['href'],
						'anchor' => $link['anchor'],
						'offset' => $link['offset'],
						'page'   => $row->link
					));
					
					if ($find)
					{
						//$this->
					}

				}
				
				if ( ! $row->words) unset($result['data'][$k]);
				
			}
		}
		
		$this->data['data'] =& $data;
	}
	
	//--------------------------------------------------------------------------
	
	function suba_view($cid)
	{
		$ofield = empty($_GET['order']) || preg_replace('/[a-z_]+/i', '', $_GET['order']) ? 'id' : $_GET['order'];
		$otype  = empty($_GET['desc']) ? '' : ' DESC';
		$this->data['ofield'] = $ofield;
		$this->data['otype']  = $otype;
		
		$ad_context = $this->db->where('id=?', $cid)->get('ad_context')->row();
		
		$links = $this->db->where('pid=?', $cid)->order_by($ofield . $otype)->get('ad_context_links')->result();
		
		/*

		if ( ! empty($_POST['remove_links']))
		{

			$data = array();
			foreach ($_POST['remove_links'] as $link)
			{
				$opt = explode('-', $link);
				$data[$opt[0]][$opt[1]][] = array(
					'pos'    => $opt[2],
					'offset' => $opt[3]
				);
			}
			
			$result = 0;
			
			foreach ($data as $type => $pages)
			{
				foreach ($pages as $row_id => $words)
				{
					$result += $this->ad->context->remove_links($cid, $type, $row_id, $words);
				}
			}
			
			if ($result)
			{
				$this->data['message'] = 'Удалено ссылок: ' . $result;
			}

		}
*/
		
		//$result     = $this->ad->context->find_links($ad_context->url);
		
		//$count = 0;
		//foreach ($result['result'] as $mod => $mod_result) foreach ($mod_result['data'] as $row) $count += $row->count;
		
		//$this->db->where('id=?', $cid)->update('ad_context', array('count'=>$count));
		
		$this->data['cid']        =& $cid;
		$this->data['ad_context'] =& $ad_context;
		$this->data['links']      =& $links;
	}
	
	//--------------------------------------------------------------------------
	
	function suba_add($cid)
	{
		if (!$cid) return FALSE;
		
		$this->data['result'] = NULL;
		if ( ! empty($_POST['query']))
		{
			$this->data['result'] = $this->ad->context->find_words(explode("\n", $_POST['query']));
		}
		
		if ( ! empty($_POST['links']))
		{
			$data = array();
			foreach ($_POST['links'] as $link)
			{
				$opt = explode('-', $link);
				$data[$opt[0]][$opt[1]][] = array(
					'pos'    => $opt[2],
					'offset' => $opt[3]
				);
			}
			
			$result = 0;
			
			foreach ($data as $type => $pages)
			{
				foreach ($pages as $row_id => $words)
				{
					$result += $this->ad->context->add_links($cid, $type, $row_id, $words);
				}
			}
			
			if ($result)
			{
				$this->data['message'] = 'Добавлено ссылок: ' . $result;
			}

		}
	}
	
	//--------------------------------------------------------------------------
	
	function suba_settings($cid)
	{
		if ( ! empty($_POST['key']))
		{
			$data = array();
			foreach ($_POST['key'] as $i => $link)
			{
				if ( ! trim($link)) continue;
				$data[$i] = array_map('trim', array_merge(array($link), explode(',', $_POST['variants'][$i])));
			}
			
			$this->db->where('id=?', $cid)->update('ad_context', array('settings'=>serialize($data)));
		}
		
		$ad_context = $this->db->where('id=?', $cid)->get('ad_context')->row();
		$settings   = unserialize($ad_context->settings);
		
		$this->data['settings']   = $settings;
		$this->data['ad_context'] = $ad_context;
	}
	
	//--------------------------------------------------------------------------
	
	function index()
	{
		$this->ad->compaigns_model->init_form();
		
		if ($this->form->validation())
		{
			$this->ad->compaigns_model->insert();
		}
		
		$this->db->order_by('status DESC, title');
		$this->data['compaings'] = $this->ad->compaigns_model->get_result();
		
		
		$this->data['stat'] = array();
		$date = new DateTime;
		$date->setTime(0, 0);
		$this->data['day0'] = $day0 = $date->format('U');
		$date->modify('-1 day');
		$this->data['day1'] = $day1 = $date->format('U');
		$date->modify('-1 day');
		$this->data['day2'] = $day2 = $date->format('U');
		$date->modify('-1 day');
		$this->data['day3'] = $day3 = $day_time = $date->format('U');
		
		$stat_result = $this->db
			->select('b.cid AS compaign_id, s.day_time, SUM(s.clicks) AS clicks, SUM(s.views) AS views')
			->join('ad_banners b', 'b.id = s.banner_id', 'LEFT')
			->order_by('s.day_time DESC')
			->group_by('s.day_time,compaign_id')
			->where('s.day_time >= ?', $day_time)
			->get('ad_stat_daily s')
			->result();

		//$stat_result = $this->db->order_by('clicks DESC, views DESC')->where_in('place_id', $where_in)->where('clicks>1')->get('ad_stat')->result();
		foreach ($stat_result as $stat) $this->data['stat'][$stat->compaign_id][$stat->day_time] = $stat;
		foreach ($this->data['compaings'] as $c)
		{
			if ( ! isset($this->data['stat'][$c->id]))
			{
				$this->data['stat'][$c->id] = array();
			}

			$row =& $this->data['stat'][$c->id];
			
			for ($i=0; $i<4; $i++)
			{
				$dvar = "day{$i}";
				if ( ! isset($row[$$dvar]))
				{
					$row[$$dvar] = new stdClass;
					$row[$$dvar]->clicks = 0;
					$row[$$dvar]->views  = 0;
				}
			}
		}
		
	}
	
	//--------------------------------------------------------------------------
	
	function act_edit($cid)
	{
		$this->db->where('id=?', $cid);
		$compaign = $this->ad->compaigns_model->get_row();
		
		$this->ad->compaigns_model->init_form();
		
		foreach ($compaign as $key => $val) $this->form->set_value($key, $val);
		
		if ($this->form->validation())
		{
			$this->db->where('id=?', $cid);
			$this->ad->compaigns_model->update();
			
			header('Location: /admin/ad/');
			exit;
		}
		
		// VIEW
		$this->data['compaign'] =& $compaign;
	}
	
	//--------------------------------------------------------------------------
	
	function act_set_compaign_status($cid, $status = 0)
	{
		$this->db->where('id=?', $cid)->update('ad_compaigns', array('status'=>(int)(bool)$status));
		
		header("Location: /admin/ad/");
		
		$this->view = FALSE;
	}
	
	//--------------------------------------------------------------------------
	
	function router($com_id, $s_act = NULL, $rel_id = 0)
	{
		$this->view = 'banners_place';
		
		$this->db->where('id = ?', $com_id);
		$compaing = $this->ad->compaigns_model->get_row();
		if ( ! $compaing) return '';

		$this->ad->banners_place_model->cid = $com_id;
		$this->ad->banners_place_model->init_form();
		
		if ($s_act && $rel_id)
		{
			switch ($s_act)
			{
				case 'remove':
					$this->db->where('id = ?', $rel_id)->limit(1);
					$this->ad->banners_place_model->delete();
					break;
				
				case 'edit':
					$this->view = 'banners_place_edit';
					$this->db->where('id = ?', $rel_id);
					
					$banner_place = $this->ad->banners_place_model->get_row();
					$this->data['banner_place'] = $banner_place;
					foreach ($banner_place as $f => $v) $this->form->set_value($f, $v);
					if ($this->form->validation())
					{
						$this->db->where('id = ?', $rel_id);
						foreach ($_POST as &$post) if (is_array($post)) $post = current($post);
						$this->ad->banners_place_model->update();
					}
					return;
					break;
			}
		}
		
		if ($this->form->validation())
		{
			$_POST['cid'] = $com_id;
			
			
			foreach ($_POST as &$post) if (is_array($post)) $post = current($post);
			$this->ad->banners_place_model->insert();
		}
		
		$this->db->order_by('bid, id');
		$this->db->where('cid=?', $com_id);
		$banners_place = $this->ad->banners_place_model->get_result();
		
		$where_in = array();
		foreach ($banners_place as $bp) $where_in[$bp->id] = $bp->id;
		
		$this->data['stat'] = array();
		if ($where_in)
		{
			$date = new DateTime;
			$date->setTime(0, 0);
			$date->modify('-2 day');
			$day_time = $date->format('U');
			
			$stat_result = $this->db->order_by('day_time DESC')->where_in('place_id', $where_in)->where('day_time >= ?', $day_time)->get('ad_stat_daily')->result();
			$stat_all    = $this->db->select('place_id, SUM(clicks) AS clicks, SUM(views) AS views')->order_by('day_time DESC')->group_by('place_id')->where_in('place_id', $where_in)->get('ad_stat_daily')->result();
			//$stat_result = $this->db->order_by('clicks DESC, views DESC')->where_in('place_id', $where_in)->where('clicks>1')->get('ad_stat')->result();
			foreach ($stat_result as $stat) $this->data['stat'][$stat->place_id][] = $stat;
			foreach ($stat_all as $stat) $this->data['stat_all'][$stat->place_id][] = $stat;
		}
		
		$this->db->where('cid=?', $com_id);
		$result = $this->ad->banners_model->get_result();
		foreach ($result as $row) $banners[$row->id] = $row;
		
		$this->data['banners']       =& $banners;
		$this->data['banners_place'] =& $banners_place;
		$this->data['model']         =& $this->ad->banners_place_model;
		$this->data['compaing']      =& $compaing;
	}
	
	//--------------------------------------------------------------------------
	
	function act_page_stat($com_id, $bp_id)
	{
		//$stat_result = $this->db->order_by('clicks DESC, views DESC')->where_in('place_id', $where_in)->where('clicks>1')->get('ad_stat')->result();
		$stat_result = $this->db->where('place_id=? AND clicks>1', $bp_id)->order_by('clicks DESC, views DESC')->get('ad_stat')->result();

		$this->data['stat'] =& $stat_result;
	}

	//--------------------------------------------------------------------------
	
	function act_stat($com_id)
	{
		error_reporting(E_ALL);	
		$date = new DateTime;
		
		if (isset($_POST['date_to']))
		{
			list($d, $m, $y) = explode('.', $_POST['date_to']);
			$date->setDate($y, $m, $d);
			$date->setTime(23, 59, 59);
		}
		$date_to = $date->format('U');
		
		if (isset($_POST['date_from']))
		{
			list($d, $m, $y) = explode('.', $_POST['date_from']);
			$date->setDate($y, $m, $d);
		}
		else
		{
			$date->modify('-6day');
		}
		$date->setTime(0, 0, 0);
		$date_from = $date->format('U');
		
			
		$this->db->where('id = ?', $com_id);
		$compaing = $this->ad->compaigns_model->get_row();
		if ( ! $compaing) return '';
		
		$b_place = $this->db->where('cid = ?', $com_id)->get('ad_banners_place')->result();
		
		$global_stat = array(
			'clicks' => 0,
			'views'  => 0,
		);
		$pb_ids = array();
		foreach ($b_place as $row)
		{
			$pb_ids[$row->id] = $row->id;
			$global_stat['clicks'] += $row->clicks;
			$global_stat['views']  += $row->views;
		}
		
		$stat = $this->db->select('day_time, SUM(clicks) AS clicks, SUM(views) AS views')
			->where_in('place_id', $pb_ids)
			->where('day_time >= ? AND day_time <= ?', $date_from, $date_to)
			->group_by('day_time')
			->get('ad_stat_daily')
			->result();
		
		$this->data['stat']     =& $stat;
		$this->data['compaing'] =& $compaing;
		$this->data['global_stat'] =& $global_stat;
		
		$this->data['date_from'] = date('d.m.Y', $date_from);
		$this->data['date_to']   = date('d.m.Y', $date_to);
	}
	
	//--------------------------------------------------------------------------
	
	function get_stat($bid)
	{
		if (isset($this->data['stat'][$bid]))
		{
			$stat = $this->data['stat'][$bid];
		}
		else
		{
			$stat = FALSE;
		}
		return $stat;
	}
	
	//--------------------------------------------------------------------------
	
	function get_stat_all($bid)
	{
		if (isset($this->data['stat_all'][$bid]))
		{
			$stat = $this->data['stat_all'][$bid][0];
		}
		else
		{
			$stat = FALSE;
		}
		return $stat;
	}
	
	//--------------------------------------------------------------------------
	
	function act_banners($com_id, $s_act = NULL, $rel_id = 0)
	{
		$this->db->where('id = ?', $com_id);
		$compaing = $this->ad->compaigns_model->get_row();
		
		if ( ! $compaing) return '';

		$this->data['sub_a']    = $s_act;
		$this->data['compaing'] =& $compaing;
		
		$this->ad->banners_model->init_form();
		
		if ($s_act && $rel_id)
		{
			switch ($s_act)
			{
				case 'remove':
					$this->db->where('id = ?', $rel_id)->limit(1);
					$this->ad->banners_model->delete();
					break;
				
				case 'edit':
					$this->view = 'banners_edit';
					$this->db->where('id = ?', $rel_id);
					$banner = $this->ad->banners_model->get_row();
					foreach ($banner as $f => $v) $this->form->set_value($f, $v);
					if ($this->form->validation())
					{
						$this->db->where('id = ?', $rel_id);
						$this->ad->banners_model->update();
					}
					return;
					break;
			}
		}
		
		if ($this->form->validation())
		{
			$_POST['cid'] = $com_id;
			$this->ad->banners_model->insert();
		}
		
		$this->db->where('cid = ?', $com_id);
		$this->data['banners']  = $this->ad->banners_model->get_result();
		
	}
	
	//--------------------------------------------------------------------------
	
}