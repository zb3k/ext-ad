<?php


class EXT_COM_Ad
{

	//--------------------------------------------------------------------------
	
	function __construct()
	{
		sys::set_base_objects(&$this);
		$this->load->database();
	}
	
	//--------------------------------------------------------------------------
	
	function b($id)
	{
		$banner = $this->db->where('id=?', $id)->get('ad_banners')->row();

		if ( ! $banner) sys::error_404();
		
		$this->db->query('UPDATE ad_banners SET clicks = clicks + 1 WHERE id = ?', $id);
		
		$this->_update_stat($id);
		
		$this->_go($id);
	}
	
	//--------------------------------------------------------------------------
	
	function _exec($com, $id, $param=array())
	{
		if ( ! is_numeric($id))
		{
			if (method_exists($this, $id))
			{
				return call_user_func_array(array($this, $id), $param);
			}
			
			sys::error_404();
		}
		
//		$id = (int)current($id);
		$bid = @$this->db->select('bid')->where('id=?', $id)->get('ad_banners_place')->row()->bid;
		
		if ( ! $bid) sys::error_404();
		
		$this->db->query('UPDATE ad_banners_place SET clicks = clicks + 1 WHERE id = ?', $id);
		
		$this->_update_stat($bid, $id);
		
		$this->_go($bid);
	}
	
	//--------------------------------------------------------------------------
	
	private function _update_stat($banner_id, $place_id = 0)
	{
		if ( ! empty($_SERVER['HTTP_REFERER']))
		{
			$url = preg_replace('/http:\/\/[^\/]*/i', '', $_SERVER['HTTP_REFERER']);
			if (preg_match('/[^\/]$/i', $url)) $url .= '/';
			$key = md5($banner_id . '_' . $place_id . '_' . $url);
			
			$this->db->query('UPDATE ad_stat SET clicks = clicks + 1 WHERE `key` = ?', $key);
			
			if ( ! $this->db->affected_rows() && $place_id)
			{
				$this->db->query('INSERT INTO ad_stat (`key`, url, place_id, clicks) VALUES (?,?,?,1) ON DUPLICATE KEY UPDATE clicks = clicks + 1', $key, $url, $place_id);
			}
		}
		
		
		$date = new DateTime;
//		$date->modify('-1 day');
		$date->setTime(0, 0);
		$day_time = $date->format('U');
		if ($place_id)
		{
			$this->db->query('UPDATE ad_stat_daily SET clicks = clicks + 1 WHERE place_id = ? AND banner_id = ? AND day_time = ?', $place_id,$banner_id, $day_time);
			
			if ( ! $this->db->affected_rows())
			{
				$this->db->query('INSERT INTO ad_stat_daily (day_time, place_id, banner_id, clicks, views) VALUES (?,?,?,1,1) ON DUPLICATE KEY UPDATE clicks = clicks + 1', $day_time, $place_id, $banner_id);
			}
		}
		else
		{
			$this->db->query('UPDATE ad_stat_daily SET clicks = clicks + 1 WHERE banner_id = ? AND day_time = ?', $banner_id, $day_time);
			
			if ( ! $this->db->affected_rows())
			{
				$this->db->query('INSERT INTO ad_stat_daily (day_time, banner_id, clicks, views) VALUES (?,?,1,1) ON DUPLICATE KEY UPDATE clicks = clicks + 1', $day_time, $banner_id);
			}
		}
		
	}
	
	//--------------------------------------------------------------------------
	
	private function _go($banner_id)
	{
		$url = $this->db->select('url')->where('b.id=?', $banner_id)->get('ad_banners b')->row()->url;
		
		header("Location: {$url}");
		exit;
	}
	
	//--------------------------------------------------------------------------
}