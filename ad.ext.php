<?php


class EXT_Ad
{
	//--------------------------------------------------------------------------
	
	private $places = array();
	private $pages  = array();
	private $limits = array();

	private $current_banners = NULL;
	private $current_places  = NULL;
//	private $trend_list      = array();
	private $views_banners   = array();
	
	public $trend     = 0;
	public $illness   = 0;
	public $component = '';
	public $relation  = 0;
	
	// public $limits = array(
	// 	'header' => 1,
	// 	'header_left' => 1,
	// 	'header_right' => 1,
	// 	'befor_ask_question' => 1,
	// 	'top_left' => 1,
	// 	'top_middle' => 1,
	// 	'top_right' => 1,
	// 	'content_symptoms' => 1,
	// 	'content_inspect' => 1,
	// 	'content_cure' => 1,
	// 	'content_prevention' => 1,
	// 	'column_top' => 3,
	// 	'column_bottom' => 3
	// );
	
	//--------------------------------------------------------------------------
	
	public function __construct()
	{
		$dir = dirname(__FILE__);
		
		// require_once $dir . '/config' . EXT;
		
		sys::load_config('ad');

		foreach (sys::$config->ext_ad->places as $key => &$place)
		{
			$this->places[$key] =& $place['name'];
			$this->limits[$key] =& $place['name'];
		}
		$this->pages =& sys::$config->ext_ad->pages;
		// exit;

		require_once $dir . '/ad_compaigns' . MODEL_EXT;
		require_once $dir . '/ad_banners' . MODEL_EXT;
		require_once $dir . '/ad_banners_place' . MODEL_EXT;
		// require_once $dir . '/ad_context' . EXT;
		
		$this->compaigns_model     = new MODEL_Ad_Compaigns;
		$this->banners_model       = new MODEL_Ad_Banners;
		$this->banners_place_model = new MODEL_Ad_Banners_Place;
		// $this->context             = new EXT_Ad_Context(&$this);
		
		$this->db =& sys::$lib->db;
	}
	
	//--------------------------------------------------------------------------
	
	public function __destruct()
	{
		if ( ! $this->views_banners) return;
		if (sys::$ext->user->group_id == 1) return;
			
		$url = '/' . sys::$lib->uri->uri_string();
		if (preg_match('/[^\/]$/i', $url)) $url .= '/';
		
		foreach ($this->views_banners as $id)
		{
			$keys[$id] = md5($id . $url);
			$ids[] = $id;
		}
		
		$this->db->query('UPDATE ad_stat SET views = views + 1 WHERE `key` IN ("' . implode('","', $keys) . '")');
		
		/*
		if ($this->db->affected_rows() != count($this->views_banners))
		{
			foreach ($keys as $id => $key)
			{
				$this->db->query('INSERT IGNORE INTO ad_stat (`key`, url, place_id, views) VALUES (?,?,?,1)', $key, $url, $id);
			}
		}
		*/
		
		$date = new DateTime;
		$date->setTime(0, 0);
		$day_time = $date->format('U');
		
		$this->db->query('UPDATE ad_stat_daily SET views = views + 1 WHERE day_time = ? AND place_id IN ("' . implode('","', $ids) . '")', $day_time);
		
		if (count($this->views_banners)) $this->db->query('UPDATE ad_banners_place SET views = views + 1 WHERE id IN ("' . implode('","', $this->views_banners) . '")');
	}
	
	//--------------------------------------------------------------------------
	
	public function load_banners()
	{		
		$this->current_banners = array();
		$this->current_places  = array();
		
		$this->component = $this->component ? $this->component : sys::$lib->router->component();
		
		// variants
		//		| 	pg	|	tr	|	il	|	$pg	|	$tr	|	$il	|
		//------|-------|-------|-------|-------|-------|-------|
		//	1	|	all	|	0	|	0	|		|		|		| - все страницы
		//	2	|	all	|	7	|	0	|		|	X	|		| - все страницы направления 7
		//	3	|	all	|	0	|	3	|		|		|	X	| - все страницы болезни 3
		//	4	|	all	|	7	|	3	|		|	X	|	X	| - все страницы направления 7 и болезни 3
		//	5	|	***	|	0	|	0	|	X	|		|		| - все страницы раздела ***
		//	6	|	***	|	7	|	0	|	X	|	X	|		| - все страницы раздела *** и направления 7
		//	7	|	***	|	0	|	3	|	X	|		|	X	| - все страницы раздела *** и болезни 3
		//	8	|	***	|	7	|	3	|	X	|	X	|	X	| - все страницы раздела ***, направления 7 и болезни 3
		
		//	$pg	|	$tr	|	$il	|
		//------|-------|-------|
		//	x	|	0	|	0	| 1 5
		//	x	|	7	|	3	| 1 2 3 4 5 6 7 8
		//	x	|	7	|	0	| 1 2 5 6
		//	x	|	0	|	3	| 1 3 5 7
		
		$t = (array)$this->trend;
		
		foreach ($t as $i => $row) if ( ! $row) unset($t[$i]);
		
		// 1 5
		
		$this->db->or_where("(page='' OR page=?) AND (trend=0 AND relation=0" . ($this->relation ? ' AND index_only=' . (int)!$this->relation : '') . ")", $this->component);
		if ($t) $this->db->or_where("(page='' OR page=?) AND (trend IN (\"" . implode('","', $t) . "\") AND relation=0" . ($this->relation ? ' AND index_only=0' : '') . ")", $this->component);
		if ($this->relation) $this->db->or_where('page=? AND relation=?', $this->component, $this->relation);

		$this->db->join('ad_compaigns c', 'c.id = p.cid AND c.status = 1', 'RIGHT');
		$this->db->select('p.*');
		$banners_place_result = $this->banners_place_model->get_result('ad_banners_place p');
		
//		echo $this->db->last_query;
		
		$where_in = array();
		
		// current_places
		foreach ($banners_place_result as $banner_place)
		{
			$this->current_places[$banner_place->place][$banner_place->id] = $banner_place;
			$where_in[$banner_place->bid] = $banner_place->bid;
		}
		
		// current_banners
		if ($where_in)
		{
			$this->db->where_in('id', $where_in);
			$banners_result = $this->banners_model->get_result();
			foreach ($banners_result as $banner)
			{
				$this->current_banners[$banner->id] = $banner;
			}
		}
	}
	
	//--------------------------------------------------------------------------
	
	public function get($place)
	{
		if (empty($this->places[$place]))
		{
			ob_get_level() && ob_clean();
			die ('AD Extension / Unknown place: ' . $place);
		}
		
		if ($this->current_places === NULL) $this->load_banners();
		
		if (empty($this->current_places[$place])) return;

		$banners_place = $this->current_places[$place];
		
		$relation_place = NULL;
		foreach ($banners_place as $bp) if ($bp->relation) $relation_place[] = $bp;
		if (count($relation_place)) $banners_place = $relation_place;
		
		if ($place != 'recommend')
		{
			shuffle($banners_place);
		}
		
		$html = '';
		
		$where_in = array();
		$loaded_banners = array();
		$counter = 0;
		foreach ($banners_place as $place_id => $bp)
		{
			if (in_array($bp->bid, $loaded_banners)) continue;
			$counter++;
			$this->views_banners[$bp->id] = $bp->id;
			$link = "/go/{$bp->id}/";
			$html .= $bp->admin_buttons . $this->render_banner($bp, $link, $place);
			
			if (isset($this->limits[$place]) && $this->limits[$place] == $counter)
			{
				break;
			}
			
			$loaded_banners[] = $bp->bid;
		}
		
		return $html;
	}
	
	//--------------------------------------------------------------------------
	
	function render_banner(&$banner_place, $link, $place)
	{
		$banner = $this->current_banners[$banner_place->bid];
		$debug  = '';
		if (sys::$ext->user->id == 1)
		{
			$debug = '<div class="ad_debug_name">'
				. "#{$banner_place->id}: [Клик: {$banner_place->clicks}] &nbsp; [Показ: {$banner_place->views}]"
				. '</div>';
		}
		
		if (strpos($banner->body, '%') !== FALSE)
		{
			$banner->body = str_replace('%LINK%', '#" onclick="return go(this, \''.$link.'\')', $banner->body);
			$banner->body = str_replace('%IMG%', "<a href=\"#\" onclick=\"return go(this, '{$link}')\"><img src=\"{$banner->img_s}\" alt=\"\"></a>", $banner->body);
			$banner->body = str_replace('%IMG_SRC%', $banner->img_s, $banner->body);
		}
		
		if ( ! $banner_place->template_id)
		{
			return "<noindex>{$banner->body}</noindex>";
		}
		
		switch ($banner_place->template_id)
		{
			case "body":
return<<<DATA
<noindex><div class="aa {$place}">{$debug}<a href="#" onclick="return go(this, '{$link}')">{$banner->body}</a></div></noindex>
DATA;
				break;

			default:
return<<<DATA
<noindex>
<div class="rounded aa {$place}">
	{$debug}
	<a href="#" onclick="return go(this, '{$link}')"><img src="{$banner->thumb}" alt=""></a>
	<a href="#" onclick="return go(this, '{$link}')">{$banner->title}</a>
	<p><a href="#" onclick="return go(this, '{$link}')">{$banner->body}</a></p>
	<a href="#" onclick="return go(this, '{$link}')" class="btn_green"><span>Подробнее</span></a>
</div>
</noindex>
DATA;

		}
	}
	
	//--------------------------------------------------------------------------
	
	public function places()
	{
		return $this->places;
	}
	
	//--------------------------------------------------------------------------
	
	public function pages()
	{
		return $this->pages;
	}
	
	//--------------------------------------------------------------------------
}