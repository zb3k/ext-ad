<?php



class MODEL_Ad_Banners_Place extends SYS_Model_Database
{
	//--------------------------------------------------------------------------
	
	//public $name  = 'Рекламные компании';
	public $table      = 'ad_banners_place';
	public $date_fomat = 'd.m.Y - H:i';
	public $cid = 0;
	
	//--------------------------------------------------------------------------
	
	function init()
	{
		$this->fields['ad_banners_place'] = array(
			'id'  => NULL,
			'bid'  => array(
				'label'   => 'Баннер',
				'field'   => 'select',
				'options' => 'banners_list',
				'rules'   => 'required'
			),
			'cid'  => NULL,
			'status' => array(
				'label'   => 'Статус',
				'default' => 1,
				'field'   => 'select',
				'options' => 'status_list',
				'user_group' => array(1)
			),
			'views' => NULL,
			'clicks' => NULL,
			'place' => array(
				'label'   => 'Место размещения',
				'field'   => 'select',
				'options' => 'place_list',
				'rules'   => 'trim|required'
			),
			'page' => array(
				'label'   => 'Раздел',
				'field'   => 'select',
				'options' => 'page_list',
				'rules'   => ''
			),
			// 'trend' => array(
			// 	'label'   => 'Направление',
			// 	'field'   => 'select',
			// 	'options' => 'trend_list'
			// ),
			'index_only' => array(
				'label' => 'Только на главной раздела',
				'field' => 'radiogroup',
				'options' => array(
					0 => 'Нет',
					1 => 'Да'
				)
			),
			'relation' => array(
				'label' => 'ID объекта',
				'field' => 'input'
			),
			'template_id' => array(
				'label'   => 'Шаблон',
				'field'   => 'select',
				'options' => 'template_list',
				// 'rules'   => 'trim|required'
			),
/*
			'postdate' => array(
				'label'   => 'Дата создания',
				'default' => time(),
			),
			'begindate' => array(
				'label'   => 'Дата начала показа',
				'default' => time(),
			),
			'enddate' => array(
				'label'   => 'Дата окончания показа',
			),
*/
		);
	}
		
	//--------------------------------------------------------------------------
	
	public function prepare_row_result(&$row)
	{
/*
		$row->postdate  = date($this->date_fomat, $row->postdate);
		$row->begindate = date($this->date_fomat, $row->begindate);
		$row->enddate   = $row->enddate ? date($this->date_fomat, $row->enddate) : 'Не ограничено';
*/
		
		return parent::prepare_row_result($row);
	}
	
	//--------------------------------------------------------------------------
	
	public function status_list($val = NULL)
	{
		static $list = array(
			0 => 'Отключен',
			1 => 'Включен'
		);
		
		if ($val !== NULL) return $list[$val];
		
		return $list;
	}

	//--------------------------------------------------------------------------
	
	public function template_list($val = NULL)
	{
		static $list;
		
		if ($list === NULL)
		{
			$list = array(
				''     => 'По умолчанию',
				// 'img'  => 'Только изображение',
				'body' => 'Только содержание',
			);
		}
		
		if ($val !== NULL) return $list[$val];
		
		return $list;
	}

	//--------------------------------------------------------------------------
	
	public function banners_list($val = NULL)
	{
		static $list;
		
		if ($list === NULL)
		{
			if ($this->cid)
			{
				$this->db->where('ad_banners.cid = ?', $this->cid);
			}
			
			$result = $this->ad->banners_model->get_result();
			foreach ($result as $row)
			{
				$list[$row->id] = $row->id . ' - ' . ($row->name ? $row->name : 'noname');
				$clist[$row->cid] = $row;
			}
			
			if ( ! $this->cid)
			{
				$this->db->where('status=1')->order_by('title');
				$result = $this->ad->compaigns_model->get_result();
				foreach ($result as $row)
				{
					$bp = $clist[$row->id];
					$group_list[$row->title][$bp->id] = $bp->id . ' - ' . ($bp->name ? $bp->name : 'noname');
				}
			}
		}
		
		if ($val !== NULL) return isset($list[$val]) ? $list[$val] : '---';
		
		return isset($group_list) ? $group_list : $list;
	}
	
	//--------------------------------------------------------------------------
	
	// public function trend_list($val = NULL)
	// {
	// 	static $list;
		
	// 	if ($list === NULL)
	// 	{
	// 		$list[] = '';
	// 		$result = $this->db->select('id, name')->order_by('name')->get('trends')->result();
	// 		foreach ($result as $row) $list[$row->id] = str_pad($row->id, 2, '0', false) . ' - ' . $row->name;
	// 	}
		
	// 	if ($val !== NULL) return $list[$val];
		
	// 	return $list;
	// }
	
	//--------------------------------------------------------------------------
	
	public function place_list($val = NULL)
	{
		static $list;
		if ($list === NULL) $list = $this->ad->places();
		if ($val !== NULL) return $list[$val];
		return $list;
	}
	
	
	//--------------------------------------------------------------------------
	
	public function page_list($val = NULL)
	{
		static $list;
		if ($list === NULL) $list = $this->ad->pages();
		if ($val !== NULL) return $list[$val];
		return $list;
	}
	
	//--------------------------------------------------------------------------
}