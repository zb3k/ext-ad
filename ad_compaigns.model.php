<?php



class MODEL_Ad_Compaigns extends SYS_Model_Database
{
	//--------------------------------------------------------------------------
	
	//public $name  = 'Рекламные компании';
	public $table = 'ad_compaigns';
	public $date_fomat = 'd.m.Y - H:i';
	
	//--------------------------------------------------------------------------
	
	function init()
	{
		$this->fields['ad_compaigns'] = array(
			'id'  => NULL,
			'status' => array(
				'label'   => 'Статус',
				'default' => 1,
				'field'   => 'select',
				'options' => 'status_list',
				'user_group' => array(1)
			),
			'title' => array(
				'label' => 'Название',
				'field' => 'input',
				'rules' => 'trim|strip_tags|required',
			),
			'postdate' => array(
				'label'   => 'Дата создания',
				'default' => time(),
			),
			'begindate' => array(
				'label'   => 'Дата начала показа',
				'default' => time(),
			),
			'enddate' => array(
				'label'   => 'Дата окончания показа',
			),
		);
	}
		
	//--------------------------------------------------------------------------
	
	public function prepare_row_result(&$row)
	{
		$row->postdate  = date($this->date_fomat, $row->postdate);
		$row->begindate = date($this->date_fomat, $row->begindate);
		$row->enddate   = $row->enddate ? date($this->date_fomat, $row->enddate) : 'Не ограничено';
		
		return parent::prepare_row_result($row);
	}
	
	//--------------------------------------------------------------------------
	
	public function status_list($val = NULL)
	{
		static $list = array(
			0 => 'Отключен',
			1 => 'Включен'
		);
		
		if ($val !== NULL) return $list[$val];
		
		return $list;
	}
	
	//--------------------------------------------------------------------------
}